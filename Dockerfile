FROM debian:buster-backports

ENV DEBIAN_FRONTEND noninteractive

RUN apt update && apt-get install -y wget gnupg apt-utils locales && \
    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8 && \
    wget -qO- https://repo.saltstack.com/py3/debian/10/amd64/latest/SALTSTACK-GPG-KEY.pub | apt-key add - && \
    echo "deb http://repo.saltstack.com/py3/debian/10/amd64/latest buster main" > /etc/apt/sources.list.d/saltstack.list && \
    apt update && apt-get install -y --no-install-recommends \
        salt-ssh \
        ssh-client \
        python3-git python3-msgpack python3-docker \
        libgit2-27 \
        python3-cffi python3-libnacl python3-pycparser python3-cherrypy3 \
        python3-jinja2 python3-mako python3-mysqldb python3-crypto python3-zmq python3-m2crypto python3-pycryptodome python3-pygit2 python3-gnupg && \
    apt-get clean && apt-get autoremove -yq --purge && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN echo '   StrictHostKeyChecking no' >> /etc/ssh/ssh_config

ENV LANG en_US.UTF-8
ENV LC_CTYPE en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

CMD ["salt-ssh", "--version"]
